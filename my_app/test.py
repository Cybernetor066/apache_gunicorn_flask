#! /usr/bin/python3.8

from tbselenium.tbdriver import TorBrowserDriver
from selenium.webdriver.common.by import By
driver = TorBrowserDriver(tbb_path="/var/www/tor-browser_en-US/", tbb_logfile_path='/dev/null', headless=True)
driver.get("https://check.torproject.org")
hello = driver.find_element(By.CLASS_NAME, "on").text
driver.quit()
print(f"{hello} Hello world!")



