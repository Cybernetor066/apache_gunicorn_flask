from tbselenium.tbdriver import TorBrowserDriver
from selenium.webdriver.common.by import By
from flask import Flask

app = Flask(__name__)

@app.route("/")
def order_process():
    driver = TorBrowserDriver(tbb_path="/var/www/tor-browser_en-US", headless=True)
    driver.get("https://check.torproject.org")
    hello = driver.find_element(By.CLASS_NAME, "on").text
    driver.quit()
    print(f"{hello} Hello world!")
    return hello


if __name__ == '__main__':
    app.run()







    