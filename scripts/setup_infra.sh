#!/bin/bash

# After configuring Apache, use this to reflect changes
sudo a2ensite flaskrest.conf
sudo ln -s /etc/apache2/sites-available/flaskrest.conf /etc/apache2/sites-enabled/


# Then restart
sudo systemctl daemon.reload


# Enable the apache proxy modules
sudo a2enmod proxy
# sudo a2enmod proxy_http
# sudo a2enmod proxy_balancer
# sudo a2enmod lbmethod_byrequests


sudo systemctl restart apache2
sudo systemctl status apache2



# <Location /> tag indicates that all requests that come to port 80 of Apache server would be passed to Gunicorn. 
# ProxyPass and ProxyPassreverse are set to pass the requests to the unix socket file we have created and configured with Gunicorn.








